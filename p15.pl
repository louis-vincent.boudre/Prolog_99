repeat(_, 0, []).
repeat(E, N, [E|Es]) :-
    N1 is N - 1,
    repeat(E, N1, Es).

dupli(_, 0, []).
dupli([],_,[]).
dupli([H|T], N, L) :-
    repeat(H, N, L1),
    append(L1, L2, L),
    dupli(T,N,L2).
