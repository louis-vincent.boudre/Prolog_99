span([X], [X], []).
span([X,Y|Zs], [X], [Y|Zs]) :- X \= Y.
span([X,X|Xs], [X|Ys], L) :- span([X|Xs], Ys, L).

encode([], []).
encode([H|T], [[H,N]|T2]) :-
    span([H|T], Ys, Zs),
    length(Ys, N),
    encode(Zs, T2).
