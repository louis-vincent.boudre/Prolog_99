take([],_,[],[]).
take(L,0,[],L).
take([X],N, 
take([X|Xs], N, [X|Ys], Zs) :-
    N1 is N-1,
    take(Xs, N1, Ys, Zs).

drop([],_,[]).
drop(L, 0, L).
drop(Xs, N, L) :-
    N > 0,
    take(Xs,N,L1,[_|L2]),
    append(L1, L3, L),
    drop(L2, N, L3).

