:- use_module(library(clpb)).

solve(A,B,C,D,E,F) :-
    sat(A =:= (B * C * D * E * F)),
    sat(B =:= (A * ~C * ~D * ~E * ~F)),
    sat(C =:= (A * B)),
    sat(D =:= card([1,2,3],[A,B,C])),
    sat(E =:= (~A * ~B * ~C * ~D)),
    sat(F =:= (~A * ~B * ~C * ~D * ~E)).
