

insert_at(X, Ys, 1, [X|Ys]) :- !.
insert_at(X, [Y|Ys], N, [Y|Zs]) :-
    N1 is N - 1,
    insert_at(X, Ys, N1, Zs).
    
