
remove_at(X, [X|Xs], 1, Xs) :- !.
remove_at(X, [X|Xs], K, [X|Zs]) :-
    K1 is K - 1,
    remove_at(X, Xs, K1, Zs).
