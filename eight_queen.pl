
flatten([], []) :- !.
flatten([queen(_,Row)|Qs], [Row|Rs]) :- flatten(Qs, Rs).

solve(N, Qs) :- 
    solve(N, [], Xs),
    flatten(Xs, Qs).

solve(0, Acc, Acc).
solve(N, Acc, Qs) :-
    N > 0,
    between(1,8,Y),
    noattack(queen(N,Y), Acc),
    N1 is N - 1,
    solve(N1, [queen(N,Y)|Acc], Qs).
    
	

noattack(queen(X,Y), Qs) :-
    \+ member(queen(X,_), Qs),
    \+ member(queen(_,Y), Qs),
    forall(
        member(queen(X1,Y1), Qs),
        (Dx is abs(X1-X), 
         Dy is abs(Y1-Y),
    	 Dx \= Dy)
    ).
