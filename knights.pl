solve(N, XYs) :-   
    between(1, N, X),
    between(1, N, Y),
    solve(N, X/Y, [X/Y], XYs).

solve(N, _, Acc, Acc) :-
    N2 is N * N,
    length(Acc, N2).
solve(N, X/Y, Acc, XYs) :-
    jump(N, X/Y, U/V),
    \+ member(U/V, Acc),
    solve(N, [U/V|Acc], XYs).

jump(N, X/Y, U/V) :-
    between(1,N,U),
    between(1,N,V),
    (2 is abs(X - U), 1 is abs(Y - V);
     1 is abs(X - U), 2 is abs(Y - V)).

