take([],_,[],[]).
take(L,0,[],L).
take([X],N, 
take([X|Xs], N, [X|Ys], Zs) :-
    N1 is N-1,
    take(Xs, N1, Ys, Zs).


