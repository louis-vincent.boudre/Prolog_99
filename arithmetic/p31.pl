is_prime(1) :- !.
is_prime(2) :- !.
is_prime(3) :- !.
is_prime(N) :-
    N1 is N-1,
    between(2, N1)
