graph([a,b,c,d], [e(a,b),e(b,c),e(c,d),e(d,a)]).

either(A,_,A).
either(_,B,B).

edge(A,B) :-
    graph(_,E),
    (member(e(A,B), E); member(e(B,A), E)).

neighbors(V, Vs) :-
    findall(V1, edge(V,V1), Vs).

is_cyclic(C, V) :-
    neighbors(V,Neighbors),
    findall(
        Node, 
            (member(Node/inc, C), 
             member(Node,Neighbors)), 
    	Nodes),
    length(Nodes,Len),
   	Len >= 2.

next_vertex(C, V) :-
    graph(G, _),
    member(V, G),
    \+ member(V/_, C).

tree(C,T, []) :-
    findall(N, member(N/inc, C), RT),
    reverse(RT, T).
tree(C, T, [V|Vs]) :-
    is_cyclic(C,V),
    tree(C,T,Vs).
tree(C, T, [V|Vs]) :-
    \+ member(V/_, C),
    \+ is_cyclic(C,V),
    either(inc, exc, S),
    tree([V/S|C], T, Vs).
   	
    
   	

