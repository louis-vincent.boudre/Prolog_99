repeat(0, _, []).
repeat(N, X, [X|Xs]) :-
    N1 is N - 1,
    repeat(N1, X, Xs).

decode_runlen([], []).
decode_runlen([X|Xs], [Z|Zs]) :-
    is_list(X),
    [N,Y] = X,
    repeat(N,Y,Z),
    decode_runlen(Xs, Zs).
decode_runlen([X|Xs], [X|Ys]) :-
    \+ is_list(X),
    decode_runlen(Xs, Ys).
